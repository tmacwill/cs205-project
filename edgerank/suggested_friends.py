import json
import sys

if len(sys.argv) < 2:
    print("Usage: python suggested_friends.py edgescore.json")
    sys.exit()

f = open(sys.argv[1], 'r')
edges = json.load(f)
f.close()

suggested_friends = {}
for user in edges:
    suggested_friends[user] = sorted(edges[user], key=edges[user].get, reverse=True)[:10]
    
print json.dumps(suggested_friends)
