import json
import csv
import os
import sqlite3
import sys

if len(sys.argv) < 3:
    print "python mr_output_to_db.py input_file output_file"
    sys.exit()

reader = csv.reader(open(sys.argv[1], 'rb'), delimiter='\t')

db_file = sys.argv[2]
if os.path.isfile(db_file):
    os.remove(db_file)

f = open(db_file, 'w')
f.write('')
f.close()

sqlite_con = sqlite3.connect(db_file)
sqlite_db = sqlite_con.cursor()
sqlite_db.execute("create table sentences (source varchar(255), next_nodes varchar(8192))")


nodes = list()
indices = {}
edges = []

for row in reader:
    if len(row) > 1:
        source = row[0]
        targets = json.loads(row[1])

        sqlite_db.execute("insert into sentences (source, next_nodes) values (?, ?)", (str(source), ','.join(targets)))
    
sqlite_con.commit()
sqlite_con.close()

