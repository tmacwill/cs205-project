import json
import sys
from collections import defaultdict

def node_dictionary_equality(d1, d2, verbose = True):
    threshold = .0000001
    if set(d1.keys()) != set(d2.keys()):
        if verbose:
            print "sources are not equal"
        return False

    for source in d1:
        if set(d1[source].keys()) != set(d2[source].keys()):
            if verbose:
                print "targets for %d are not equal" % source
            return False
        for target in d1[source]:
            if abs(d1[source][target] - d2[source][target]) > threshold:
                if verbose:
                    print "%s to %s is %f, but should be %f" % (source, target, d1[source][target], d2[source][target])
                return False

    return True

if __name__ == '__main__':
    if len(sys.argv) != 3:
        print 'Usage: test_json.py file1 file2'
        sys.exit()

    file1 = open(sys.argv[1])
    if not file1:
        print "Failed to open %s" % sys.argv[1]
        sys.exit()

    file2 = open(sys.argv[2])
    if not file2:
        print "Failed to open %s" % sys.argv[2]
        sys.exit()

# should wrap in try catch
    d1 = json.load(file1)
    d2 = json.load(file2)
    if node_dictionary_equality(d1,d2):
        print "EQUAL"
    else:
        print "NOT EQUAL"

