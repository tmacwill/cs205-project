import json
import sqlite3
import sys
from datetime import *
from dateutil.parser import parse
import time

def time_decay(time1, time2):
    delay = time2 - time1
    return (float(1)/delay)**32


if len(sys.argv) < 4:
    print "Usage: python newsfeed_serial.py edgescores.json db user_id"
    sys.exit()

# open edgescore json
f = open(sys.argv[1], 'r')
data = json.load(f)
f.close()

user_id = sys.argv[3]
affinities = data[user_id]
db_file = sys.argv[2]

start_time = time.time()

con = sqlite3.connect(db_file + ".db")
db = con.cursor()

db = con.cursor()
db.execute("select id, user_id, timestamp from posts")
posts = db.fetchall()

# fetch replies
db.execute("select replies.id, replies.user_id, replies.post_id, posts.user_id, replies.timestamp, replies.action from replies join posts on replies.post_id = posts.id")
replies = db.fetchall()

# fetch helpfuls
db.execute("select votes.id, votes.user_id, votes.reply_id, replies.user_id, replies.timestamp, votes.action from votes join replies on replies.id = votes.reply_id")
votes = db.fetchall()

# fetch watches
db.execute("select post_watches.id, post_watches.user_id, post_watches.post_id, posts.user_id, posts.timestamp, post_watches.action from post_watches join posts on post_watches.post_id = posts.id")
watches = db.fetchall()

# fetch reads
db.execute("select post_reads.id, post_reads.user_id, post_reads.post_id, posts.user_id, posts.timestamp, post_reads.action from post_reads join posts on post_reads.post_id = posts.id")
reads = db.fetchall()

# scores[id1][id2] is the edge weight score between users id1 and id2
weights = {'read':1, 'reply':1, 'watch':1, 'vote':1}
scores = []

actions = replies + votes +  watches
for action in actions:
    # skip if user is source
    if action[1] == user_id:
        continue
    # skip if user is own target
    if action[1] == action[3]:
        continue

    if not str(action[1]) in affinities:
        continue
    if not str(action[3]) in affinities:
        continue

    scores.append({'source':action[1], 'target':action[3], 'action':action[5], 'id':action[0], 'timestamp':action[4], 'score':affinities[str(action[3])] * affinities[str(action[1])] * weights[action[5]] * time_decay(int(parse(action[4]).strftime('%s')), int(datetime.now().strftime('%s')))})

display = sorted(scores, key=lambda k:k['score'], reverse=True)[:30]

#print "Total time: %f" % (time.time() - start_time)

print json.dumps(display)
