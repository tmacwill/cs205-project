import json
import os
import sqlite3
import sys
import time

# make sure enough arguments are given
if len(sys.argv) < 3:
    print "python path_serial.py ../output/network.json [json|sql] [db]"
    sys.exit()

# open graph json
f = open(sys.argv[1], 'r')
data = json.load(f)
f.close()

# clear previous database
db_file = 'paths.db'
if sys.argv[2] == 'sql':
    db_file = sys.argv[3] + '.db'
    if os.path.isfile(db_file):
        os.remove(db_file)
    f = open(db_file, 'w')
    f.write('')
    f.close()

start_time = time.time()

# connect to sqlite
sqlite_con = sqlite3.connect(db_file)
sqlite_db = sqlite_con.cursor()
sqlite_db.execute("create table paths (source int, target int, path varchar(8192))")

# parse graph json
nodes = []
edges = {}
for node in data['nodes']:
    nodes.append(node['id'])

for edge in data['edges']:
    s = int(edge['source'])
    t = int(edge['target'])

    if not s in edges:
        edges[s] = {}
    if not t in edges[s]:
        edges[s][t] = 1

# compute shortest path over all nodes
paths = {}
for start in nodes:
    # initialize queue and distances list
    distances = {}
    previous = {}
    queue = []
    for node in nodes:
        distances[node] = sys.maxint
        previous[node] = sys.maxint
        queue.append(node)

    # distance to start node is 0
    distances[start] = 0

    # explore nodes until end is found
    while len(queue) > 0:
        min_index = 0
        min_value = sys.maxint

        # determine node with minimum weight
        for i, v in enumerate(queue):
            if distances[v] < min_value:
                min_index = i
                min_value = distances[v]

        # remove minimum node from queue
        node = queue[min_index]
        del queue[min_index]

        # determine if distance to new node is better than the current distance
        d = distances[node] + 1
        if node in edges:
            for n in edges[node]:
                if d < distances[n]:
                    distances[n] = d
                    previous[n] = node

    # backtrack to find path to all end nodes
    for end in nodes:
        if start != end:
            # step through previous nodes array
            n = end
            path = []
            while n != sys.maxint and previous[n]:
                path.insert(0, str(n))
                n = previous[n]

            # store path
            if not start in paths:
                paths[start] = {}
            paths[start][end] = path
            
            # save path in database
            if sys.argv[2] == 'sql':
                sqlite_db.execute("insert into paths (source, target, path) values (?, ?, ?)", 
                    (str(start), str(end), ','.join(path)))

#print "Total time: %f" % (time.time() - start_time)

# output json
if sys.argv[2] == 'json':
    print json.dumps({
        'paths': paths
    })

# close sqlite connection
elif sys.argv[2] == 'sql':
    sqlite_con.commit()
    sqlite_con.close()
