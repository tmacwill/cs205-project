import json
import sqlite3
import sys
import math
from datetime import *
from dateutil.parser import parse
from mpi4py import MPI
import numpy as np

def time_decay(time1, time2):
    delay = time2 - time1
    return float(1)/delay


actions = None
counts = {}

# initialize MPI
comm = MPI.COMM_WORLD
rank = comm.Get_rank()
size = comm.Get_size()

# make sure enough arguments are given
if len(sys.argv) < 2:
    if rank == 0:
        print "Usage: mpirun -n [2|4|8] python edgescore_mpi.py db"
    sys.exit()

db_file = sys.argv[1]

# start timer
comm.barrier()
parallel_start_time = MPI.Wtime()

con = sqlite3.connect(db_file + ".db")
db = con.cursor()

# get the total number of posts
db.execute("select count(id) from posts")
counts['post'] = db.fetchone()[0]

# get the total number of posts
db.execute("select count(id) from replies")
counts['reply'] = db.fetchone()[0]

# get the total number of posts
db.execute("select count(id) from post_watches")
counts['watch'] = db.fetchone()[0]

# get the total number of posts
db.execute("select count(id) from post_reads")
counts['read'] = db.fetchone()[0]

# get the total number of posts
db.execute("select count(id) from votes")
counts['vote'] = db.fetchone()[0]

starts = {}
ends = {}
for count in counts:
    # each process handles a subset of threads
    n = int(np.ceil(float(counts[count]) / size))
    start = n * rank
    end = n * rank + n
    if rank == size - 1:
        end = counts[count] + 1
    starts[count] = start
    ends[count] = end


db = con.cursor()
db.execute("select id, user_id, timestamp from posts where posts.id >= %d and posts.id < %d " % (starts['post'], ends['post']))
posts = db.fetchall()

# fetch replies
db.execute("select replies.id, replies.user_id, replies.post_id, posts.user_id, replies.timestamp, replies.action from replies join posts on replies.post_id = posts.id where replies.id >= %d and replies.id < %d" % (starts['reply'], ends['reply']))
replies = db.fetchall()

# fetch helpfuls
db.execute("select votes.id, votes.user_id, votes.reply_id, replies.user_id, replies.timestamp, votes.action from votes join replies on replies.id = votes.reply_id where votes.id >= %d and votes.id < %d" % (starts['vote'], ends['vote']))
votes = db.fetchall()

# fetch watches
db.execute("select post_watches.id, post_watches.user_id, post_watches.post_id, posts.user_id, posts.timestamp, post_watches.action from post_watches join posts on post_watches.post_id = posts.id where post_watches.id >= %d and post_watches.id < %d" % (starts['watch'], ends['watch']))
watches = db.fetchall()

# fetch reads
db.execute("select post_reads.id, post_reads.user_id, post_reads.post_id, posts.user_id, posts.timestamp, post_reads.action from post_reads join posts on post_reads.post_id = posts.id where post_reads.id >= %d and post_reads.id < %d" % (starts['read'], ends['read']))
reads = db.fetchall()

actions = replies + votes + reads + watches


# scores[id1][id2] is the edge weight score between users id1 and id2
weights = {'read':0.2, 'reply':0.3, 'watch':0.4, 'vote':0.3}
summed_scores = {}
for action in actions:
    if not action[1] in summed_scores:
        summed_scores[action[1]] = {}
    if not action[3] in summed_scores[action[1]]:
        summed_scores[action[1]][action[3]] = 0
    summed_scores[action[1]][action[3]] += weights[action[5]] * time_decay(int(parse(action[4]).strftime('%s')), int(datetime.now().strftime('%s')))


# using a log(n) reduction, consolidate subscores into a single score array
for i in xrange(1, int(math.log(size, 2) + 1)):
    # after sending a subgraph once, process is done
    if rank % 2**(i-1) == 0:
        # send subgraph to adjacent active process
        if rank % 2**i != 0 and rank-2**(i-1) >= 0:
            comm.send(summed_scores, dest=rank-2**(i-1))
        # receive subgraph from adjacent active process
        elif rank+2**(i-1) < size:
            recv_scores = comm.recv(source=rank+2**(i-1))
            # combine subgraphs by summing scores
            for source in recv_scores:
                scores = recv_scores[source]
                for target in scores:
                    if not source in summed_scores:
                        summed_scores[source] = {}
                    if not target in summed_scores[source]:
                        summed_scores[source][target] = 0
                    summed_scores[source][target] += recv_scores[source][target]
    # make sure all processes finish before moving on
    comm.barrier()

if rank == 0:
    # total parallel time
    parallel_end_time = MPI.Wtime()
    #print "Total time: %f" % (parallel_end_time - parallel_start_time)

    print json.dumps(summed_scores)
