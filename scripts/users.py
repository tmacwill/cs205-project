import json
import sqlite3
import sys
from collections import defaultdict

if __name__ == '__main__':
    if len(sys.argv) < 2:
        print "Usage: python users db"
        sys.exit()

    db_file = sys.argv[1]

    con = sqlite3.connect(db_file)
    db = con.cursor()

    db = con.cursor()
    db.execute("select id, name from users")
    data = db.fetchall()
    users_list = {}
    for user in data:
        users_list[user[0]] = user[1];
    print json.dumps({
        'users': users_list
    })
