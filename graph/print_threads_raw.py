import sqlite3
import json
import sys

def process_thread(nodes, edges):
    # convert the thread to a single line with all words separated by spaces
    # print each thread to a different line for processing by MapReduce
    text = ""
    print sys.stdout.encoding
    for node in thread:
        text = text + " " + node

    text = text.strip()
    text = text.replace("\n", " ")
    text = text.replace("\t", " ")
    text = text.replace("(", " ")
    text = text.replace(")", " ")
    text = ' '.join(text.split())

    if len(text) > 0:
        try:
            f.write(text + "\n")
        except UnicodeEncodeError:
            print "Couldn't encode a symbol"

# open the database and read in posts
db_file = 'discuss50.db'
con = sqlite3.connect(db_file)
db = con.cursor()

db = con.cursor()
db.execute("select posts.id, posts.title, posts.content from posts")
data = db.fetchall()

f = open('discuss50_raw.txt', 'w')

# process all threads in the database
nodes = set()
edges = {}
previous_id = 0
thread = set()
for i in data:
    if i[0] != previous_id:
        process_thread(nodes, edges)
        thread = set()
        thread.add(i[1])

    thread.add(i[2])
    previous_id = i[0]
process_thread(nodes, edges)

