import json
import sys
from collections import defaultdict

def node_dictionary_equality(d1, d2, verbose = True):
    # check nodes
    nodes1 = {}
    nodes2 = {}
    for node in d1['nodes']:
        nodes1[node['id']] = node
    for node in d2['nodes']:
        nodes2[node['id']] = node

    if set(nodes1.keys()) != set(nodes2.keys()):
        if verbose:
            print "node ids are not equal"
            print set(nodes1.keys()) ^ set(nodes2.keys())
        return False

    for id in nodes1:
        if nodes1[id]['weight'] != nodes2[id]['weight']:
            if verbose:
                print "weight on node %d is %d, but should be %d" % (id, nodes1[id]['weight'], nodes2[id]['weight'])
            return False
    # check edges
    edges1 = defaultdict(lambda: defaultdict(int))
    edges2 = defaultdict(lambda: defaultdict(int))
    for edge in d1['edges']:
        edges1[edge['source']][edge['target']] = edge['weight']
    for edge in d2['edges']:
        edges2[edge['source']][edge['target']] = edge['weight']

    if set(edges1.keys()) != set(edges2.keys()):
        if verbose:
            print "sources are not equal"
        return False

    for source in edges1:
        if set(edges1[source].keys()) != set(edges2[source].keys()):
            if verbose:
                print "targets for %d are not equal" % source
            return False
        for target in edges1[source]:
            if edges1[source][target] != edges2[source][target]:
                if verbose:
                    print "%d to %d is %d, but should be %d" % (source, target, edges1[source][target], edges2[source][target])
                return False

    return True

if __name__ == '__main__':
    if len(sys.argv) != 3:
        print 'Usage: test_json.py file1 file2'
        sys.exit()

    file1 = open(sys.argv[1])
    if not file1:
        print "Failed to open %s" % sys.argv[1]
        sys.exit()

    file2 = open(sys.argv[2])
    if not file2:
        print "Failed to open %s" % sys.argv[2]
        sys.exit()

# should wrap in try catch
    d1 = json.load(file1)
    d2 = json.load(file2)
    if node_dictionary_equality(d1,d2):
        print "EQUAL"
    else:
        print "NOT EQUAL"

