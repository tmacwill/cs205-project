$(function() {
    // get data
    $.getJSON('../output/suggested_friends.json', function(scores) {
        $.getJSON('../output/users.json', function(users) {
            // build select box
            var html = '<option value=""></option>';
            for (var i in scores)
                html += '<option value="' + i + '">' + users.users[i] + '</option>';
            $('#users').html(html);

            $('#users').on('change', function() {
                var friends = scores[$(this).val()];

                // build suggestions
                var html = '';
                for (var i in friends)
                    html += '<li>' + users.users[friends[i]] + '</li>';

                $('#people').html(html);
            });
        });
    });
});
