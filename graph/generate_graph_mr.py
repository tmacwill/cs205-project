import sqlite3
import json
import time

from mrjob.job import MRJob
import math

class MRConnections(MRJob):
    def mapper(self, key, text):
        # yield each pair of words in the text as a tuple of the 2 words
        words = text.split(' ')
        for w1 in words:
            if w1 and w1 != 'null':
                for w2 in words: 
                    if w1 != w2 and w2 and w2 != 'null':
                        yield (w1,w2), 1

    def reducer(self, edge, c):
        # sum the weights for each edge
        yield edge, sum(c)

if __name__ == '__main__':
    MRConnections.run()
