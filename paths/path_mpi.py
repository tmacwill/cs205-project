import json
import os
import sqlite3
import sys
import numpy as np
from mpi4py import MPI
import time

# initialize MPI
comm = MPI.COMM_WORLD
rank = comm.Get_rank()
size = comm.Get_size()

# make sure enough arguments are given
if len(sys.argv) < 4:
    if rank == 0:
        print "Usage: mpirun -n 8 python path_mpi.py ../output/network.json [paths|weights] [json|sql] [db]"
    sys.exit()

# clear previous database
db_file = 'paths.db'
if rank == 0:
    if sys.argv[3] == 'sql':
        db_file = sys.argv[4] + '.db'
        if os.path.isfile(db_file):
            os.remove(db_file)
        f = open(db_file, 'w')
        f.write('')
        f.close()

# start timer
comm.barrier()
parallel_start_time = MPI.Wtime()

# connect to sqlite
sqlite_con = sqlite3.connect(db_file)
sqlite_db = sqlite_con.cursor()
if rank == 0:
    sqlite_db.execute("create table paths (source int, target int, path varchar(8192))")

# open graph json
comm.barrier()
f = open(sys.argv[1], 'r')
data = json.load(f)
f.close()

# parse graph json
nodes = []
edges = {}
for node in data['nodes']:
    nodes.append(node['id'])

for edge in data['edges']:
    s = int(edge['source'])
    t = int(edge['target'])
    if not s in edges:
        edges[s] = {}
    if not t in edges[s]:
        edges[s][t] = 1

# determine which nodes to compute shortest paths for
n = int(np.ceil(float(len(nodes)) / size))
p_start = n * rank
p_end = n * rank + n
if rank == size - 1:
    p_end = len(nodes)

# compute shortest path over all nodes
paths = {}
for s in xrange(p_start, p_end):
    # initialize queue and distances list
    start = nodes[s]
    distances = {}
    previous = {}
    queue = []
    for node in nodes:
        distances[node] = sys.maxint
        previous[node] = sys.maxint
        queue.append(node)

    # distance to start node is 0
    distances[start] = 0

    # explore nodes until end is found
    while len(queue) > 0:
        min_index = 0
        min_value = sys.maxint

        # determine node with minimum weight
        for i, v in enumerate(queue):
            if distances[v] < min_value:
                min_index = i
                min_value = distances[v]

        # remove minimum node from queue
        node = queue[min_index]
        del queue[min_index]

        # determine if distance to new node is better than the current distance
        d = distances[node] + 1
        if node in edges:
            for n in edges[node]:
                if d < distances[n]:
                    distances[n] = d
                    previous[n] = node

    # backtrack to find path to all end nodes
    for end in nodes:
        if start != end:
            # step through previous nodes array
            n = end
            path = []
            while n != sys.maxint and previous[n]:
                path.insert(0, str(n))
                n = previous[n]

            # store path
            if not start in paths:
                paths[start] = {}
            paths[start][end] = path

# send all paths to gathering process
gathered_paths = comm.gather(paths)

if rank == 0:
    # total parallel time
    parallel_end_time = MPI.Wtime()
    serial_start_time = MPI.Wtime()

    type = sys.argv[2]
    data = sys.argv[3]

    if type == 'paths':
        # save paths in sqlite database
        if data == 'sql':
            for paths in gathered_paths:
                for s, v in paths.iteritems():
                    for t, p in v.iteritems():
                        sqlite_db.execute("insert into paths (source, target, path) values (?, ?, ?)", (s, t, ','.join(p)))

            # close sqlite connection
            sqlite_con.commit()
            sqlite_con.close()

        # output paths as json
        elif data == 'json':
            all_paths = {}
            for paths in gathered_paths:
                for k, v in paths.iteritems():
                    all_paths[k] = v

            print json.dumps({
                'paths': all_paths
            })

    # compute lengths of paths
    elif type == 'weights':
        all_weights = {}
        for paths in gathered_paths:
            for s, v in paths.iteritems():
                for t, p in v.iteritems():
                    if not s in all_weights:
                        all_weights[s] = {}
                    all_weights[s][t] = len(p) - 1

        print json.dumps({
            'weights': all_weights
        })

    serial_end_time = MPI.Wtime()
    #print "Total parallel time: %f" % (parallel_end_time - parallel_start_time)
    #print "Total serial time: %f" % (serial_end_time - serial_start_time)
    #print "Total time: %f" % (serial_end_time - parallel_start_time)
