$(function() {
    // get list of all source words
    $.getJSON('sentences.php?type=all', function(response) {
        // create typeahead from large words list
        $('#txt-word').typeahead({
            source: response.words
        });

        // get next words when next is clicked
        $('#btn-word').on('click', function() {
            $.getJSON('sentences.php?type=word&source=' + $('#txt-word').val(), function(response) {
                // create suggestions list
                var html = '';
                for (var i = 0; i < response.words.length; i++)
                    html += '<li><a href="#">' + response.words[i] + '</a></li>';

                // add word to sentence
                $('#sentence').text($('#sentence').text() + ' ' + $('#txt-word').val());

                // show list and clear word
                $('#suggestions').html(html);
                $('#txt-word').val('');

            });
        });

        // get next n words in sentence
        $('#btn-lucky').on('click', function() {
            var n = 6;
            $('#sentence').text($('#sentence').text() + ' ' + $('#txt-word').val());

            $.getJSON('sentences.php?type=lucky&n=' + n + '&source=' + $('#txt-word').val(), function(response) {
                // add words to sentence
                for (var i = 0; i < response.words.length - 1; i++)
                    $('#sentence').text($('#sentence').text() + ' ' + response.words[i]);

                // put last word in text box
                $('#txt-word').val(response.words[response.words.length - 1]);
            });
        });

        // use word when it is selected
        $('#suggestions').on('click', 'a', function() {
            // put word into text box and click next
            $('#txt-word').val($(this).text());
            $('#btn-word').click();
        });
    });
});
