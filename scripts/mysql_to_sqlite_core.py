import MySQLdb as mysql
import sqlite3
import os
import sys

if len(sys.argv) < 2:
   print "Usage: mysql_to_sqlite [db]"
   sys.exit()

db_file = sys.argv[1] + '.db'

if os.path.isfile(db_file):
   os.remove(db_file)

f = open(db_file, 'w')
f.write('')
f.close()

mysql_con = mysql.connect('localhost', 'root', '', sys.argv[1]);
mysql_db = mysql_con.cursor()
sqlite_con = sqlite3.connect(db_file)
sqlite_db = sqlite_con.cursor()

mysql_db.execute("select id, name from users order by id asc")
data = mysql_db.fetchall()

sqlite_db.execute("create table users (id int, name varchar(255), primary key (id))")

for i in data:
   query = "insert into users (id, name) values (?, ?)"
   sqlite_db.execute(query, (i[0], i[1]))


sqlite_con.commit()
sqlite_con.close()
