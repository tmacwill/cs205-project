import sqlite3
import json
from collections import defaultdict
import math
import numpy as np
import sys
from mpi4py import MPI
import os

def process_thread(nodes, edges):
    """
    Nodes are words and directed edges indicate that one word precedes another
    """

    # get all words in the thread
    words = []
    for node in thread:
        if node != None:
            words = words + node.split(' ')

    # iterate over all pairs of successive words
    words = filter(None, words)
    for i in range(0, len(words)-1):
        w1 = words[i].lower()
        w2 = words[i+1].lower()

        # add each node once to the set
        nodes.add(w1)
        if (i == len(words)-2):
            nodes.add(w2)

        # update edges
        if w1 and w1 != 'null' and w2 and w2 != 'null':
            # make sure elements exist in dictionary
            if not w1 in edges:
                edges[w1] = {}
            if not w2 in edges[w1]:
                edges[w1][w2] = 0

            # increment number of connections
            edges[w1][w2] += 1


# initialize MPI
comm = MPI.COMM_WORLD
rank = comm.Get_rank()
size = comm.Get_size()

# make sure enough arguments are given
if len(sys.argv) < 3:
    if rank == 0:
        print "Usage: mpirun -n [2|4|8] python sentence_predictor_mpi.py input_db output_db [min_threshold] [max_threshold]"
    sys.exit()


# parse command-line arguments
db_file = sys.argv[1] + ".db"
out_file = sys.argv[2] + ".db"
min_threshold = 0
max_threshold = sys.maxint
if len(sys.argv) > 3:
    min_threshold = int(sys.argv[3])
if len(sys.argv) > 4:
    max_threshold = int(sys.argv[4])

# start timer
comm.barrier()
parallel_start_time = MPI.Wtime()

# connect to sqlite
con = sqlite3.connect(db_file)
db = con.cursor()

# get the total number of posts
db.execute("select count(id) from posts")
post_count = db.fetchone()[0]

# each process handles a subset of threads
n = int(np.ceil(float(post_count) / size))
start = n * rank
end = n * rank + n
if rank == size - 1:
    end = post_count + 1

# query for user data for social network
query = "select posts.id, posts.title, posts.content, replies.content from posts left outer join replies on posts.id = replies.post_id where posts.id >= %d and posts.id < %d" % (start, end)

# run data-specific query
db.execute(query)
data = db.fetchall()

# process each thread
thread = set()
nodes = set()
edges = {}
previous_id = 0
for i in data:
    # if we have a new thread ID, then we have accumulated all replies, so we can process the thread
    if previous_id != i[0]:
        process_thread(nodes, edges)
        thread = set()

        thread.add(i[1])
        thread.add(i[2])

    thread.add(i[3])

    # remember thread id
    previous_id = i[0]

# handle the final thread
process_thread(nodes, edges)

# flatten the 2d edge list into a 1d list
nodes_list = list(nodes)
summed_edges = edges

# using a log(n) reduction, consolidate subgraphs into a single adjacency list
for i in xrange(1, int(math.log(size, 2) + 1)):
    # after sending a subgraph once, process is done
    if rank % 2**(i-1) == 0:
        # send subgraph to adjacent active process
        if rank % 2**i != 0 and rank-2**(i-1) >= 0:
            comm.send(summed_edges, dest=rank-2**(i-1))
        # receive subgraph from adjacent active process
        elif rank+2**(i-1) < size:
            recv_edges = comm.recv(source=rank+2**(i-1))
            # combine subgraphs by summing edges
            for source in recv_edges:
                edges = recv_edges[source]
                for target in edges:
                    if not source in summed_edges:
                        summed_edges[source] = {}
                    if not target in summed_edges[source]:
                        summed_edges[source][target] = 0
                    summed_edges[source][target] += recv_edges[source][target]
    # make sure all processes finish before moving on
    comm.barrier()

# gather all nodes at once
gathered_nodes = comm.gather(nodes_list)
all_nodes = set()
if rank == 0:
    # total parallel time
    parallel_end_time = MPI.Wtime()
    serial_start_time = MPI.Wtime()

    # make database
    if os.path.isfile(out_file):
        os.remove(out_file)

    f = open(out_file, 'w')
    f.write('')
    f.close()

    sqlite_con = sqlite3.connect(out_file)
    sqlite_db = sqlite_con.cursor()
    sqlite_db.execute("create table sentences (source varchar(255), next_nodes varchar(8192))")
    
    # compute the set of unique nodes
    for i in gathered_nodes:
        all_nodes |= set(i)

    # for each node, find the closest 10 nodes and write to table
    for node in all_nodes:
        sorted_nodes = []
        if node in summed_edges:
            sorted_edges = sorted(summed_edges[node])
            top_words = sorted_edges[:min(len(sorted_edges),10)]
            
            sqlite_db.execute("insert into sentences (source, next_nodes) values (?, ?)", 
                (str(node), ','.join(top_words)))

    serial_end_time = MPI.Wtime()
    print "Total parallel time: %f" % (parallel_end_time - parallel_start_time)
    print "Total serial time: %f" % (serial_end_time - serial_start_time)
    print "Total time: %f" % (serial_end_time - parallel_start_time)

    sqlite_con.commit()
    sqlite_con.close()
