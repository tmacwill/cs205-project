import sqlite3
import json

from mrjob.job import MRJob
import math

class MRConnections(MRJob):
    def mapper(self, key, text):
        # yield every pair of successive words in the text
        # first word as the key, second as the value
        words = text.split(' ')
        words = filter(None, words)
        for i in range(0, len(words)-1):
            w1 = words[i]
            w2 = words[i+1]
            if w1 and w1 != 'null' and w2 and w2 != 'null':
                yield w1.lower(), w2.lower()

    def reducer(self, start, end):
        # combine all targets with an edge from the start node
        edges = dict()
        for n in end:
            if n in edges:
                edges[n] += 1
            else:
                edges[n] = 1
        
        # sort the nodes and return the closest 10
        sorted_nodes = []
        count = 0
        for n, wgt in sorted(edges.iteritems(), key=lambda(k,v): (v,k), reverse=True):
            count += 1
            sorted_nodes.append(n)
            if count >= 10:
                break

        # yield the start node and the list of the top 10 successors
        if count > 0:
            yield start, sorted_nodes


if __name__ == '__main__':
    MRConnections.run()
