import MySQLdb as mysql
import sqlite3
import os
import sys
from BeautifulSoup import BeautifulSoup

if len(sys.argv) < 3 or (not (sys.argv[2] == "raw" or sys.argv[2] == "clean")):
    print "Usage: mysql_to_sqlite [db] [raw|clean]"
    sys.exit()

db_file = sys.argv[1] + '.db'

clean = sys.argv[2] == "clean"

if os.path.isfile(db_file):
    os.remove(db_file)

f = open(db_file, 'w')
f.write('')
f.close()

mysql_con = mysql.connect('localhost', 'root', '', sys.argv[1]);
mysql_db = mysql_con.cursor()
sqlite_con = sqlite3.connect(db_file)
sqlite_db = sqlite_con.cursor()

mysql_db.execute("select id, user_id, title, content, timestamp from posts where suite_id = 6 order by id asc")
data = mysql_db.fetchall()

sqlite_db.execute("create table posts (id int, user_id int, title varchar(255), content varchar(8192), action varchar(255), `timestamp` timestamp, primary key (id))")
sqlite_db.execute("create table replies (id int, user_id int, post_id int, content varchar(8192), action varchar(255),  `timestamp` timestamp, primary key(id))")
sqlite_db.execute("create table post_watches (id int, user_id int, post_id int, action varchar(255),  primary key(id))")
sqlite_db.execute("create table post_reads (id int, user_id int, post_id int, action varchar(255),  primary key(id))")
sqlite_db.execute("create table votes (id int, user_id int, reply_id int, action varchar(255),  primary key(id))")

post_ids = []
id = 1
new_post_ids = {}
for i in data:
    post_ids.append(str(i[0]))
    new_post_ids[i[0]] = id
    query = "insert into posts (id, user_id, title, content, action, timestamp) values (?, ?, ?, ?,'post', ?)"
    text = i[3]
    if clean:
        text = ''.join(BeautifulSoup(i[3], convertEntities=BeautifulSoup.HTML_ENTITIES).findAll(text=True)).encode('ascii', 'ignore')
    sqlite_db.execute(query, (id, i[1], i[2], text, i[4]))
    id += 1
post_ids = ','.join(post_ids)

mysql_db.execute("select id, user_id, post_id, content,timestamp from replies where post_id in (" + post_ids + ") order by id asc")
data = mysql_db.fetchall()

reply_ids = []
id = 1
new_reply_ids = {}
for i in data:
    reply_ids.append(str(i[0]))
    new_reply_ids[i[0]] = id
    query = "insert into replies (id, user_id, post_id, content, action, timestamp) values (?, ?, ?, ?, 'reply', ?)"
    text = i[3]
    if clean:
        text = ''.join(BeautifulSoup(i[3], convertEntities=BeautifulSoup.HTML_ENTITIES).findAll(text=True)).encode('ascii', 'ignore')
    sqlite_db.execute(query, (id, i[1], i[2], text, i[4]))
    id += 1
reply_ids = ','.join(reply_ids)

mysql_db.execute("select id, user_id, post_id from post_watches where post_id in (" + post_ids + ") order by id asc")
data = mysql_db.fetchall()

id = 1
for i in data:
    query = "insert into post_watches (id, user_id, post_id, action) values (?, ?, ?, 'watch')"
    sqlite_db.execute(query, (id, i[1], new_post_ids[i[2]]))
    id += 1 

mysql_db.execute("select id, user_id, post_id from post_reads where post_id in (" + post_ids + ") order by id asc")
data = mysql_db.fetchall()

id = 1
for i in data:
    query = "insert into post_reads (id, user_id, post_id, action) values (?, ?, ?, 'read')"
    sqlite_db.execute(query, (id, i[1], new_post_ids[i[2]]))
    id += 1 

mysql_db.execute("select id, user_id, reply_id from votes where reply_id in (" + reply_ids + ") order by id asc")
data = mysql_db.fetchall()

id = 1
for i in data:
    query = "insert into votes (id, user_id, reply_id, action) values (?, ?, ?, 'vote')"
    sqlite_db.execute(query, (id, i[1], new_reply_ids[i[2]]))
    id += 1 

sqlite_con.commit()
sqlite_con.close()
