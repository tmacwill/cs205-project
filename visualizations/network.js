$(function() {
    /**
     * Draw the given graph
     *
     */
    function draw(graph, threshold, layout) {
        if (threshold === undefined)
            threshold = 0;

        if (layout === undefined)
            layout = 0;

        $('#graph').empty();
        var root = document.getElementById('graph');
        grapher = sigma.init(root);

        var added = {};
        for (var i = 0; i < graph.nodes.length; i++) {
            if (parseInt(graph.nodes[i].weight) >= threshold) {
                var id = graph.nodes[i].id;
                added[id] = true;

                // create a random color
                var r = parseInt(Math.random() * 255);
                var g = parseInt(Math.random() * 255);
                var b = parseInt(Math.random() * 255);

                // add node to graph
                grapher.addNode(graph.nodes[i].id, {
                    label: window.users[graph.nodes[i].id] + ', ' + graph.nodes[i].id,
                    x: Math.random(),
                    y: Math.random(),
                    size: 5,
                    color: 'rgb(' + r + ', ' + g + ', ' + b + ')'
                });
            }
        }
     
        // add edges between nodes
        for (var i = 0; i < graph.edges.length; i++)
            if (added[graph.edges[i].source] && added[graph.edges[i].target])
                grapher.addEdge(graph.edges[i].source + ',' + graph.edges[i].target, 
                    graph.edges[i].source, graph.edges[i].target);

        grapher.bind('overnodes', function(event) {
            var nodes = event.content;
            var neighbors = {};
            grapher.iterEdges(function(e) {
                if (nodes.indexOf(e.source) >= 0 || nodes.indexOf(e.target) >= 0) {
                    neighbors[e.source] = 1;
                    neighbors[e.target] = 1;
                }
            }).iterNodes(function(n) {
                if (!neighbors[n.id])
                    n.hidden = 1;
                else
                    n.hidden = 0;
            }).draw(2, 2, 2);
        }).bind('outnodes', function() {
            grapher.iterEdges(function(e) {
                e.hidden = 0;
            }).iterNodes(function(n) {
                n.hidden = 0;
            }).draw(2, 2, 2);
        });

        grapher.draw();

        if (layout == 0)
            grapher.circleLayout();
        else
            grapher.randomLayout();
    }

    /**
     * Create a linear threshold slider
     *
     */
    function linearThreshold(value, max) {
        return parseInt(value / 100 * max);
    }

    /**
     * Create a logarithmic threshold slider
     *
     */
    function logThreshold(value, max) {
        return parseInt(Math.exp(Math.log(max) / 100 * parseInt(value)));
    }

    /**
     * Circular layout
     * http://sigmajs.org/examples/a_plugin_example.html
     *
     */
    sigma.publicPrototype.circleLayout = function() {
        var R = 100;
        var i = 0;
        var L = this.getNodesCount();

        this.iterNodes(function(n) {
            n.x = Math.cos(Math.PI * (i++) / L) * R;
            n.y = Math.sin(Math.PI * (i++) / L) * R;
        });

        return this.position(0, 0, 1).draw();
    };

    /**
     * Random layout
     * http://sigmajs.org/examples/a_plugin_example.html
     *
     */
    sigma.publicPrototype.randomLayout = function() {
        var W = 100;
        var H = 100;

        this.iterNodes(function(n) {
            n.x = W * Math.random();
            n.y = H * Math.random();
        });

        return this.position(0, 0, 1).draw();
    };

    // size visualization to viewport
    $('.sigma-parent').height($(window).height() - 15);

    // load network data
    $.getJSON('../output/network.json', function(graph) {
        $.getJSON('../output/users.json', function(users) {
            var users = users.users;
            window.users = users;

            // compute maximum node weight and build select boxes
            var maxWeight = 0;
            var select = '';
            for (var i = 0; i < graph.nodes.length; i++) {
                maxWeight = Math.max(maxWeight, graph.nodes[i].weight);

                if (graph.nodes[i].weight > 20)
                    select += '<option value="' + graph.nodes[i].id + '">' + users[graph.nodes[i].id] + '</option>';
            }

            window.graph = graph;
            draw(graph);

            // create auto-completes
            $('#filter-user, #path-start, #path-end').html(select).select2();

            // create slider
            $('#threshold-slider').slider({
                min: 1,
                max: 100,
                slide: function(event, ui) {
                    window.threshold = ($('#threshold-log').is(':checked')) ? logThreshold(ui.value, maxWeight) : 
                        linearThreshold(ui.value, maxWeight);
                    $('#threshold-value').text(window.threshold);
                },
                stop: function(event, ui) {
                    window.threshold = ($('#threshold-log').is(':checked')) ? logThreshold(ui.value, maxWeight) : 
                        linearThreshold(ui.value, maxWeight);
                    draw(window.graph, window.threshold, window.layout);
                }
            });

            // toggle between log and linear scales
            $('#threshold-log').on('click', function() {
                // recompute threshold with new scale
                var value = $('#threshold-slider').slider('value');
                var threshold = ($('#threshold-log').is(':checked')) ? logThreshold(value, maxWeight) : 
                    linearThreshold(value, maxWeight);
                $('#threshold-value').text(threshold);
                window.threshold = threshold;

                draw(window.graph, window.threshold, window.layout);
            });

            // enable circular layout
            $('#btn-circle').on('click', function() {
                window.layout = 0;
                draw(window.graph, window.threshold, window.layout);
                return false;
            });

            // enable random layout
            $('#btn-random').on('click', function() {
                window.layout = 1;
                draw(window.graph, window.threshold, window.layout);
                return false;
            });

            // filter the graph by a node id
            $('#btn-filter-user').on('click', function() {
                var id = $('#filter-user').val()
                var neighbors = {};

                draw(window.graph, window.threshold, window.layout);
                grapher.iterEdges(function(e) {
                    if (e.source == id || e.target == id) {
                        neighbors[e.source] = 1;
                        neighbors[e.target] = 1;
                    }
                }).iterNodes(function(n) {
                    if (!neighbors[n.id])
                        n.hidden = 1;
                    else
                        n.hidden = 0;
                }).draw(2, 2, 2);
            });
        
            // display shortest path
            $('#btn-path').on('click', function() {
                var start = $('#path-start').val();
                var end = $('#path-end').val();

                $.getJSON('paths.php?source=' + start + '&target=' + end, function(response) {
                    var html = '';
                    for (var i = 0; i < response.path.length; i++)
                        html += '<li>' + window.users[response.path[i]] + '</li>';

                    if (!html)
                        html = 'Those two users are not connected!';

                    $('#path-results').html(html);
                    return false;
                });
            });
        });
    });
});
