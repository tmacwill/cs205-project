import json
import sqlite3
import sys
import time

def process_thread(mode, nodes, edges):
    """
    Process a thread for either users or words
    """

    if mode == 'users':
        return process_thread_users(nodes, edges)
    elif mode == 'words':
        return process_thread_words(nodes, edges)


def process_thread_users(nodes, edges):
    """
    Nodes are users and edges are interactions between users
    """

    # iterate over all pairs of users
    for node in thread:
        if node and node != 'null':
            nodes.add(node)
            for n in thread:
                if node != n and n and n != 'null':
                    # make sure elements exist in dictionary
                    if not node in edges:
                        edges[node] = {}
                    if not n in edges[node]:
                        edges[node][n] = 0

                    # increment number of connections
                    edges[node][n] += 1


def process_thread_words(nodes, edges):
    """
    Nodes are words and edges are connections between words
    """

    # get all words in the thread
    words = []
    for node in thread:
        if node:
            words = words + node.split(' ')

    # iterate over all pairs of words
    for w1 in words:
        if w1 and w1 != 'null':
            nodes.add(w1)
            for w2 in words: 
                if w1 != w2 and w2 and w2 != 'null':
                    # make sure elements exist in dictionary
                    if not w1 in edges:
                        edges[w1] = {}
                    if not w2 in edges[w1]:
                        edges[w1][w2] = 0

                    # increment number of connections
                    edges[w1][w2] += 1


# make sure enough arguments are given
if len(sys.argv) < 3:
    print "Usage: python generate_graph_serial.py [users|words] db"
    sys.exit()

# parse command-line arguments
mode = sys.argv[1]
db_file = sys.argv[2]
min_threshold = 0
max_threshold = sys.maxint
if len(sys.argv) > 3:
    min_threshold = int(sys.argv[3])
if len(sys.argv) > 4:
    max_threshold = int(sys.argv[4])

# connect to sqlite
con = sqlite3.connect(db_file + ".db")
db = con.cursor()

# start timing
start_time = time.time()

# query for user data for social network
query = ''
if mode == 'users':
    query = "select posts.id, posts.user_id, replies.user_id from posts left outer join replies on posts.id = replies.post_id"

# query for post data for word graph
elif mode == 'words':
    query = "select posts.id, posts.title, posts.content, replies.content from posts left outer join replies on posts.id = replies.post_id"

# run data-specific query
db.execute(query)
data = db.fetchall()

# process each thread
nodes = set()
edges = {}
previous_id = 0
thread = set()
for i in data:
    # if we have a new thread ID, then we have accumulated all replies, so we can process the thread
    if i[0] != previous_id:
        process_thread(mode, nodes, edges)
        thread = set()

        # add post data
        if mode == 'users':
            thread.add(i[1])
        elif mode == 'words':
            thread.add(i[1])
            thread.add(i[2])

    # accumulate reply data
    if mode == 'users':
        thread.add(i[2])
    elif mode == 'words':
        thread.add(i[3])

    # remember thread id
    previous_id = i[0]

# handle the final thread
process_thread(mode, nodes, edges)

# format nodes for json output
nodes_list = []
for node in nodes:
    nodes_list.append({
        'id': node
    });

# format edges for json output
formatted_edges = []
weights = {}
for node in edges:
    for n in edges[node]:
        if edges[node][n] >= min_threshold and edges[node][n] <= max_threshold:
            formatted_edges.append({
                'source': str(node),
                'target': str(n),
                'weight': edges[node][n]
            })

        # compute total node weights
        if not str(node) in weights:
            weights[str(node)] = 0
        weights[str(node)] += edges[node][n]

# assign weights to nodes
for node in nodes_list:
    if str(node['id']) in weights:
        node['weight'] = weights[str(node['id'])]
    else:
        node['weight'] = 0

#print "Total time: %f" % (time.time() - start_time)

print json.dumps({
    'nodes': nodes_list,
    'edges': formatted_edges
})
