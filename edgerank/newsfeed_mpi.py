import json
import sqlite3
import sys
import math
from datetime import *
from dateutil.parser import parse
from mpi4py import MPI
import numpy as np

def time_decay(time1, time2):
    delay = time2 - time1
    return (float(1)/delay)**32


actions = None
counts = {}

# initialize MPI
comm = MPI.COMM_WORLD
rank = comm.Get_rank()
size = comm.Get_size()

if len(sys.argv) < 4:
    if rank == 0:
        print "Usage: mpirun -n [2|4|8] python newsfeed_mpi.py edgescore.json db user_id"
    sys.exit()

# open edgescore json
f = open(sys.argv[1], 'r')
data = json.load(f)
f.close()

user_id = sys.argv[3]
affinities = data[user_id]
db_file = sys.argv[2]

# start timer
comm.barrier()
parallel_start_time = MPI.Wtime()

con = sqlite3.connect(db_file + ".db")
db = con.cursor()

# get the total number of posts
db.execute("select count(id) from posts")
counts['post'] = db.fetchone()[0]

# get the total number of posts
db.execute("select count(id) from replies")
counts['reply'] = db.fetchone()[0]

# get the total number of posts
db.execute("select count(id) from post_watches")
counts['watch'] = db.fetchone()[0]

# get the total number of posts
db.execute("select count(id) from post_reads")
counts['read'] = db.fetchone()[0]

# get the total number of posts
db.execute("select count(id) from votes")
counts['vote'] = db.fetchone()[0]

starts = {}
ends = {}
for count in counts:
    # each process handles a subset of threads
    n = int(np.ceil(float(counts[count]) / size))
    start = n * rank
    end = n * rank + n
    if rank == size - 1:
        end = counts[count] + 1
    starts[count] = start
    ends[count] = end


db = con.cursor()
db.execute("select id, user_id, timestamp from posts where posts.id >= %d and posts.id < %d " % (starts['post'], ends['post']))
posts = db.fetchall()

# fetch replies
db.execute("select replies.id, replies.user_id, replies.post_id, posts.user_id, replies.timestamp, replies.action from replies join posts on replies.post_id = posts.id where replies.id >= %d and replies.id < %d" % (starts['reply'], ends['reply']))
replies = db.fetchall()

# fetch helpfuls
db.execute("select votes.id, votes.user_id, votes.reply_id, replies.user_id, replies.timestamp, votes.action from votes join replies on replies.id = votes.reply_id where votes.id >= %d and votes.id < %d" % (starts['vote'], ends['vote']))
votes = db.fetchall()

# fetch watches
db.execute("select post_watches.id, post_watches.user_id, post_watches.post_id, posts.user_id, posts.timestamp, post_watches.action from post_watches join posts on post_watches.post_id = posts.id where post_watches.id >= %d and post_watches.id < %d" % (starts['watch'], ends['watch']))
watches = db.fetchall()

# fetch reads
db.execute("select post_reads.id, post_reads.user_id, post_reads.post_id, posts.user_id, posts.timestamp, post_reads.action from post_reads join posts on post_reads.post_id = posts.id where post_reads.id >= %d and post_reads.id < %d" % (starts['read'], ends['read']))
reads = db.fetchall()

actions = replies + votes + reads + watches

weights = {'read':0.2, 'reply':0.3, 'watch':0.4, 'vote':0.3}

scores = []

actions = replies + votes +  watches
for action in actions:
    # skip if user is source
    if action[1] == user_id:
        continue
    # skip if user is own target
    if action[1] == action[3]:
        continue

    if not str(action[1]) in affinities:
        continue
    if not str(action[3]) in affinities:
        continue

    scores.append({'source':action[1], 'target':action[3], 'action':action[5], 'id':action[0], 'timestamp':action[4], 'score':affinities[str(action[3])] * affinities[str(action[1])] * weights[action[5]] * time_decay(int(parse(action[4]).strftime('%s')), int(datetime.now().strftime('%s')))})

# flatten lists of list into one list
gathered_scores = comm.gather(scores, root=0)
if rank == 0:
    gathered_scores = sum(gathered_scores, [])
    display = sorted(gathered_scores, key=lambda k:k['score'], reverse=True)[:30]

    # total parallel time
    parallel_end_time = MPI.Wtime()
    #print "Total time: %f" % (parallel_end_time - parallel_start_time)

    print json.dumps(display)
