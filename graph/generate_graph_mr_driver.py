from generate_graph_mr import MRConnections
import sys
import time

start_time = time.time()

mr_job = MRConnections()
with mr_job.make_runner() as runner:
    # run job
    runner.run()

sys.stderr.write("Total time: %f\n" % (time.time() - start_time))
