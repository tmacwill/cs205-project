import sqlite3
import json
from collections import defaultdict
import math
import numpy as np
import sys
import os
import time

def process_thread(nodes, edges):
    """
    Nodes are words and directed edges indicate that one word precedes another
    """

    # get all words in the thread
    words = []
    for node in thread:
        if node != None:
            words = words + node.split(' ')

    # iterate over all pairs of successive words
    words = filter(None, words)
    for i in range(0, len(words)-1):
        w1 = words[i].lower()
        w2 = words[i+1].lower()

        # add each node once to the set
        nodes.add(w1)
        if (i == len(words)-2):
            nodes.add(w2)

        # update edges
        if w1 and w1 != 'null' and w2 and w2 != 'null':
            # make sure elements exist in dictionary
            if not w1 in edges:
                edges[w1] = {}
            if not w2 in edges[w1]:
                edges[w1][w2] = 0

            # increment number of connections
            edges[w1][w2] += 1


# make sure enough arguments are given
if len(sys.argv) < 3:
    if rank == 0:
        print "Usage: python sentence_predictor_serial.py input_db output_db"
    sys.exit()

# parse command-line arguments
db_file = sys.argv[1] + ".db"
out_file = sys.argv[2] + ".db"

# start timer
start_time = time.time()

# connect to sqlite
con = sqlite3.connect(db_file)
db = con.cursor()

# query for user data for social network
query = "select posts.id, posts.title, posts.content, replies.content from posts left outer join replies on posts.id = replies.post_id"

# run data-specific query
db.execute(query)
data = db.fetchall()

# process each thread
thread = set()
nodes = set()
edges = {}
previous_id = 0
for i in data:
    # if we have a new thread ID, then we have accumulated all replies, so we can process the thread
    if previous_id != i[0]:
        process_thread(nodes, edges)
        thread = set()

        thread.add(i[1])
        thread.add(i[2])

    thread.add(i[3])

    # remember thread id
    previous_id = i[0]

# handle the final thread
process_thread(nodes, edges)

# flatten the 2d edge list into a 1d list
nodes_list = list(nodes)

# make database
if os.path.isfile(out_file):
    os.remove(out_file)

f = open(out_file, 'w')
f.write('')
f.close()

sqlite_con = sqlite3.connect(out_file)
sqlite_db = sqlite_con.cursor()
sqlite_db.execute("create table sentences (source varchar(255), next_nodes varchar(8192))")

# for each node, find the closest 10 nodes and write to table
for node in nodes_list:
    sorted_nodes = []
    if node in edges:
        sorted_edges = sorted(edges[node])
        top_words = sorted_edges[:min(len(sorted_edges),10)]
        
        sqlite_db.execute("insert into sentences (source, next_nodes) values (?, ?)", 
            (str(node), ','.join(top_words)))

print "Total time: %f" % (time.time() - start_time)

sqlite_con.commit()
sqlite_con.close()
