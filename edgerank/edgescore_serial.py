import json
import sqlite3
import sys
from datetime import *
from dateutil.parser import parse
import time

def time_decay(time1, time2):
    delay = time2 - time1
    return float(1)/delay


if len(sys.argv) < 2:
    print "Usage: python edgescore_serial.py db"
    sys.exit()

db_file = sys.argv[1]

# start timer
start_time = time.time()

con = sqlite3.connect(db_file + ".db")
db = con.cursor()

db = con.cursor()
db.execute("select id, user_id, timestamp from posts")
posts = db.fetchall()

# fetch replies
db.execute("select replies.id, replies.user_id, replies.post_id, posts.user_id, replies.timestamp, replies.action from replies join posts on replies.post_id = posts.id")
replies = db.fetchall()

# fetch helpfuls
db.execute("select votes.id, votes.user_id, votes.reply_id, replies.user_id, replies.timestamp, votes.action from votes join replies on replies.id = votes.reply_id")
votes = db.fetchall()

# fetch watches
db.execute("select post_watches.id, post_watches.user_id, post_watches.post_id, posts.user_id, posts.timestamp, post_watches.action from post_watches join posts on post_watches.post_id = posts.id")
watches = db.fetchall()

# fetch reads
db.execute("select post_reads.id, post_reads.user_id, post_reads.post_id, posts.user_id, posts.timestamp, post_reads.action from post_reads join posts on post_reads.post_id = posts.id")
reads = db.fetchall()

# scores[id1][id2] is the edge weight score between users id1 and id2
weights = {'read':0.2, 'reply':0.3, 'watch':0.4, 'vote':0.3}
scores = {}

actions = replies + votes + reads + watches
for action in actions:
    if not action[1] in scores:
        scores[action[1]] = {}
    if not action[3] in scores[action[1]]:
        scores[action[1]][action[3]] = 0
    scores[action[1]][action[3]] += weights[action[5]] * time_decay(int(parse(action[4]).strftime('%s')), int(datetime.now().strftime('%s')))

#print "Total time: %f" % (time.time() - start_time)
print json.dumps(scores)
