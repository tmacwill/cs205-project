<!doctype html>
<html>
    <head>
        <title>CS205 Final Project</title>

        <link rel="stylesheet" type="text/css" href="bootstrap.min.css" />

        <script type="text/javascript" src="jquery-1.8.3.min.js"></script>
        <script type="text/javascript" src="bootstrap.min.js"></script>

        <style>

img {
    display: block;
    margin: 10px auto;
    text-align: center;
}

        </style>
    </head>
    <body style="padding: 40px" data-spy="scroll">
        <div class="navbar navbar-fixed-top">
            <div class="navbar-inner">
                    <ul class="nav">
                        <li><a href="#problem">Problem & Data</a></li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                Implementation <b class="caret"></b>
                            </a>
                            <ul class="dropdown-menu">
                                <li><a href="#network">Social Network</a></li>
                                <li><a href="#paths">Shortest Paths</a></li>
                                <li><a href="#edgerank">EdgeRank</a></li>
                                <li><a href="#sentence">Sentence Prediction</a></li>
                            </ul>
                        </li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                Results <b class="caret"></b>
                            </a>
                            <ul class="dropdown-menu">
                                <li><a href="#user-results">User Graph Construction</a></li>
                                <li><a href="#word-results">Word Graph Construction</a></li>
                                <li><a href="#paths-results">Shortest Paths</a></li>
                                <li><a href="#edgerank-results">EdgeRank</a></li>
                                <li><a href="#newsfeed-results">News Feed</a></li>
                                <li><a href="#sentence-results">Sentence Predictor</a></li>
                            </ul>
                        </li>
                        <li><a href="#discussion">Discussion</a></li>
                        <li><a href="#future">Future Work</a></li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                Visualizations <b class="caret"></b>
                            </a>
                            <ul class="dropdown-menu">
                                <li><a href="../visualizations/network.html" target="_blank">Social Network</a></li>
                                <li><a href="../visualizations/suggestions.html" target="_blank">Suggested Friends</a></li>
                                <li><a href="../visualizations/newsfeed.html" target="_blank">News Feed</a></li>
                                <li><a href="../visualizations/sentences.html" target="_blank">Sentence Predictor</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div style="padding-left: 40px; padding-right: 40px">
            <h1 style="margin-bottom: 30px">
                Consolidating a Rich Instructional System:<br />
                Combining Educational Computation with Killer Analysis<br />
                <i>(CRIS: CECKA)</i>
            </h1>
            <h2 id="problem">Problem & Data</h1>
            <p>CS50 Discuss is CS50's in-house discussion forum used among undergraduates taking CS50 on campus and distance students taking CS50x (offered through the edX online education platform) alike. On Discuss, students ask questions of course staff and interact with their peers using an interface similar to that of GMail. Discuss has been used by over 900 on-campus students over the course of the fall semester and over 120,000 online students over the past two months. Our project seeks to analyze relationships among the users of Discuss as well as the contents of the board's posts and replies using parallel programming techniques in MapReduce and MPI.</p>
            <p>Our data comes from the MySQL database that powers the online discussion board. Each Discuss thread consists of a single post, which often represents the question asked by a student, and multiple replies, which often represent staff answering questions or other students asking clarifying questions. Associated with both posts and replies are the authoring users, the creation timestamp, and of course, the text contents. Discuss also allows students to "watch" a post that may be important to them, just as users can "star" important emails in a variety of email applications, so our data set also includes a list of posts that were watched by each user. Similarly, our data also includes which posts were read by each users and when. Finally, Discuss allows users to mark replies as "helpful," creating a karma system among users. Our data set includes which replies were marked helpful by users and when.</p>
            <img src="discuss-inbox.png" style="width: 900px" />
            <img src="discuss-thread.png" style="width: 900px" />
            <h2 id="implementation">Implementation</h2>
            <p>Our project code is implemented in Python and incorporates both MapReduce (using MRJob) and MPI (using mpi4py). To make our data set as portable as possible, Discuss data was converted to several SQLite databases, and irrelevant data was stripped out. The project's Python files output either JSON or SQLite, depending on how the data will be used in the accompanying visualizations.</p>
            <h3 id="network">Social Network</h3>
            <p>The social network graph describes which users have interacted with each other on Discuss. Each node in the graph represents a single user. Two nodes are connected if those two users have participated in the same discussion thread (e.g., one user creates a post and the other responds to it), and the weight of each edge is the number of unique threads in which the users have interacted. Each node has an associated score that defines how active a user is on the discussion board, which is defined by the sum of the weights of all connected edges.</p>
            <p>The construction of this graph can be parallelized using MPI. After determining the total number of posts in the data set via a single SQL query, each process is responsible for constructing a subgraph using only a subset of threads. When dividing the data among processes, work must be split up in terms of entire threads, not individual replies, so each subgraph can be constructed completely independently. Once all subgraphs have been connected, the adjacency lists representing the graphs are gathered into a single graph using the reduction shown below. Given two adjacency lists from different processes, the combined graph can be computed by taking the union of the two lists, summing the weights of edges that exist in both subgraphs. Each process need only perform one send, and the total number of steps necessary is log(<i>n</i>), where <i>n</i> is the total number of processes. We accomplish this by employing a tree-like reduction strategy, as demonstrated in the image below.</p>
            <img src="MPI_Tree_diagram.png" />
            <p>This problem can also be parallelized using MapReduce. The mapper operates on threads and emits pairs of users as keys, and 1s as values. To compute edge weights, the reducer then need only perform a summation.</p>
            <p>A visualization of the social network graph can be viewed <a href="../visualizations/network.html">here</a>. As the graph is quite large, the slider at top-right can be used to hide nodes whose scores fall below the specified threshold. Either a logarithmic or linear scale can be used, as CS50's Discuss data revealed that the top posters have dramatically larger scores than average users. Hovering over a node will hide all nodes that are not directly connected to that node. The same effect can be achieved using the dropdown menu below the threshold slider, which allows users to select a node by name and hide unconnected nodes.</p>
            <h3 id="paths">Shortest Paths</h3>
            <p>Once the social network graph has been constructed, Dijkstra's algorithm can be used to compute the shortest path between any two nodes in the graph. The result of this computation is CS50's equivalent to <a href="http://en.wikipedia.org/wiki/Six_Degrees_of_Kevin_Bacon" target="_blank">Six Degrees of Kevin Bacon</a> or <a href="http://en.wikipedia.org/wiki/Erd%C5%91s_number" target="_blank">Erdos number</a>.</p>
            <p>This computation of the shortest paths among all pairs of nodes in the graph is <strike>embarrassingly</strike> pleasantly parallel. Given the adjacency list of the social network, each process computes the shortest path between a subset of nodes and all other nodes in the graph. Because all paths are independent and need not be aggregated, each process can independently insert paths into a SQLite database (which supports concurrent writes) without the need to gather data into a single process. An interface for browsing shortest paths can be found <a href="../visualizations/network.html">here</a>.</p>
            <h3 id="edgerank">EdgeRank</h3>
            <p>Websites like Facebook and GMail have a similar social network graph, and both companies have developed algorithms that determine how closely connected any two nodes in the graph are. Google's algorithm used for suggested contacts in GMail can be found <a href="http://www.icml-2011.org/papers/X6_crossconference.pdf" target="_blank">here</a>, and Facebook's algorithm is described <a href="http://whatisedgerank.com" target="_blank">here</a>.</p>
            <p>Using the ideas published by Google and Facebook, we have developed our own EdgeRank algorithm that describes the connectedness of two users. Given a source node in the graph, we first compute affinity scores between the source and all other users that have interacted with that node. The affinity score takes into account both the amount of time since two users have interacted as well as the type of interaction between the two users; for example, replying to a post by another user is weighted more heavily than is reading a post by another user. Combining the scores from all of a pair of user's concurrent actions results in a final affinity score for a pair of users. We have parallelized this computation with MPI, using an approach similar to that taken in the graph construction problem. An interface for browsing users' "Suggested Friends," defined as those nodes with the highest affinity scores for a user, can be found <a href="../visualizations/suggestions.html">here</a></p></p>
            <p>Given an affinity score for a user, we can easily compute a list of relevant actions that have been taken on the discussion board. We compute a relevancy score for an action by considering the affinity between the target user and the users involved in the action, as well as the type of action, and the time since the action occured. We compute this score for all actions across all users. This list of actions is similar to Facebook's "News Feed" that users see upon logging on to the site's home page. Because action scores include a highly weighted time decay term, our Discuss news feed includes the most recent actions, so we achieve a similar result to that of Facebook. However, our implementation is also similar to Google's algorithm because our affinity scores rely on implicit, one-directional relations (e.g., reading a post), where Facebook's implementation uses explicit relations via confirmed, bi-directional friendships. An interface for browsing the news feeds for various users can be found <a href="../visualizations/newsfeed.html">here</a></p>
            <h3 id="sentence">Sentence Predictor</h3>
            <p>In addition to constructing a graph of connected users, we also constructed a word graph using using both MPI and MapReduce. In this graph, each node represents a word, and two words are connected if they are adjacent in the text of a post or reply. Again, the weight of each edge is the number of times the connected nodes have occurred next to each other in the contents of a post or reply. For a given node in this graph, we can easily compute a list of the top <i>n</i> words that commonly occur next to that word.</p>
            <p>An interface for creating sentences based on the corpus of text data can be found <a href="../visualizations/sentences.html" target="_blank">here</a>. Start by entering a word into the autocomplete-enabled text box. After selecting a word, selecting "next word" will show the top 10 words that commonly occur next to that word. Clicking on a suggested word will then display the top 10 suggestions for that word, allowing users to build sentences. Or, clicking on "I'm feeling lucky" will select the next 5 words in the sentence by choosing a random word from the top 10 suggestions.</p>
            <h2 id="results">Results</h2>
            <h3 id="user-results">User Graph Construction</h3>
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>Implementation</th>
                        <th>Processes</th>
                        <th>Total Runtime</th>
                        <th>Parallel Sub-time</th>
                        <th>Serial Sub-time</th>
                        <th>Efficiency</th>
                        <th>Speedup</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>Serial</td>
                        <td>1</td>
                        <td>0.979482</td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>MapReduce</td>
                        <td></td>
                        <td>0.979482</td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>MPI</td>
                        <td>2</td>
                        <td>1.265431</td>
                        <td>1.157075</td>
                        <td>0.108356</td>
                        <td>0.387015</td>
                        <td>0.774030</td>
                    </tr>
                    <tr>
                        <td>MPI</td>
                        <td>4</td>
                        <td>1.236728</td>
                        <td>1.126023</td>
                        <td>0.110704</td>
                        <td>0.197999</td>
                        <td>0.791995</td>
                    </tr>
                    <tr>
                        <td>MPI</td>
                        <td>8</td>
                        <td>1.259164</td>
                        <td>1.147721</td>
                        <td>0.111443</td>
                        <td>0.097235</td>
                        <td>0.777883</td>
                    </tr>
                    <tr>
                        <td>MPI</td>
                        <td>16</td>
                        <td>1.620066</td>
                        <td>1.509535</td>
                        <td>0.110529</td>
                        <td>0.037787</td>
                        <td>0.604594</td>
                    </tr>
                </tbody>
            </table>
            <img src="chart_1.png" />
            <h3 id="word-results">Word Graph Construction</h3>
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>Implementation</th>
                        <th>Processes</th>
                        <th>Total Runtime</th>
                        <th>Parallel Sub-time</th>
                        <th>Serial Sub-time</th>
                        <th>Efficiency</th>
                        <th>Speedup</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>Serial</td>
                        <td>1</td>
                        <td>734.186851</td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>MPI</td>
                        <td>2</td>
                        <td>539.388608</td>
                        <td>327.762312</td>
                        <td>211.626294</td>
                        <td>0.680573</td>
                        <td>1.361146</td>
                    </tr>
                    <tr>
                        <td>MPI</td>
                        <td>4</td>
                        <td>474.14558</td>
                        <td>261.747479</td>
                        <td>212.398099</td>
                        <td>0.3871104</td>
                        <td>1.5484418</td>
                    </tr>
                </tbody>
            </table>
            <h3 id="paths-results">Shortest Paths</h3>
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>Implementation</th>
                        <th>Processes</th>
                        <th>Total Runtime</th>
                        <th>Parallel Sub-time</th>
                        <th>Serial Sub-time</th>
                        <th>Efficiency</th>
                        <th>Speedup</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>Serial</td>
                        <td>1</td>
                        <td>113.770576</td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>MPI</td>
                        <td>2</td>
                        <td>65.32014</td>
                        <td>57.762412</td>
                        <td>7.557727</td>
                        <td>0.870869</td>
                        <td>1.741738</td>
                    </tr>
                    <tr>
                        <td>MPI</td>
                        <td>4</td>
                        <td>38.296109</td>
                        <td>30.355006</td>
                        <td>7.941102</td>
                        <td>0.742703</td>
                        <td>2.970813</td>
                    </tr>
                    <tr>
                        <td>MPI</td>
                        <td>8</td>
                        <td>33.260962</td>
                        <td>25.623318</td>
                        <td>7.637643</td>
                        <td>0.427568</td>
                        <td>3.420544</td>
                    </tr>
                    <tr>
                        <td>MPI</td>
                        <td>16</td>
                        <td>33.606232</td>
                        <td>25.317555</td>
                        <td>8.288675</td>
                        <td>0.211588</td>
                        <td>3.385401</td>
                    </tr>
                </tbody>
            </table>
            <img src="chart_2.png" />
            <h3 id="edgerank-results">EdgeRank</h3>
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>Implementation</th>
                        <th>Processes</th>
                        <th>Total Runtime</th>
                        <th>Efficiency</th>
                        <th>Speedup</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>Serial</td>
                        <td>1</td>
                        <td>38.434814</td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>MPI</td>
                        <td>2</td>
                        <td>21.116604</td>
                        <td>0.9100614</td>
                        <td>1.8201229</td>
                    </tr>
                    <tr>
                        <td>MPI</td>
                        <td>4</td>
                        <td>12.44874</td>
                        <td>0.771862</td>
                        <td>3.087446</td>
                    </tr>
                    <tr>
                        <td>MPI</td>
                        <td>8</td>
                        <td>11.540147</td>
                        <td>0.4163163</td>
                        <td>3.3305307</td>
                    </tr>
                    <tr>
                        <td>MPI</td>
                        <td>16</td>
                        <td>12.669584</td>
                        <td>0.1896018</td>
                        <td>3.0336287</td>
                    </tr>
                </tbody>
            </table>
            <img src="chart_3.png" />
            <h3 id="newsfeed-results">News Feed</h3>
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>Implementation</th>
                        <th>Processes</th>
                        <th>Total Runtime</th>
                        <th>Efficiency</th>
                        <th>Speedup</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>Serial</td>
                        <td>1</td>
                        <td>11.170749</td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>MPI</td>
                        <td>2</td>
                        <td>7.820652</td>
                        <td>0.714183</td>
                        <td>1.428365</td>
                    </tr>
                    <tr>
                        <td>MPI</td>
                        <td>4</td>
                        <td>5.501237</td>
                        <td>0.507647</td>
                        <td>2.030589</td>
                    </tr>
                    <tr>
                        <td>MPI</td>
                        <td>8</td>
                        <td>5.071765</td>
                        <td>0.275317</td>
                        <td>2.202537</td>
                    </tr>
                    <tr>
                        <td>MPI</td>
                        <td>16</td>
                        <td>6.676114</td>
                        <td>0.154577</td>
                        <td>1.673241</td>
                    </tr>
                </tbody>
            </table>
            <img src="chart_4.png" />
            <h3 id="sentence-results">Sentence Predictor</h3>
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>Implementation</th>
                        <th>Processes</th>
                        <th>Total Runtime</th>
                        <th>Parallel Sub-time</th>
                        <th>Serial Sub-time</th>
                        <th>Efficiency</th>
                        <th>Speedup</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>Serial</td>
                        <td>1</td>
                        <td>9.15716</td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>MapReduce</td>
                        <td></td>
                        <td>43.09661</td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>MPI</td>
                        <td>2</td>
                        <td>9.370942</td>
                        <td>3.843478</td>
                        <td>5.527463</td>
                        <td>0.488593</td>
                        <td>0.977187</td>
                    </tr>
                    <tr>
                        <td>MPI</td>
                        <td>4</td>
                        <td>7.642571</td>
                        <td>3.717472</td>
                        <td>3.925097</td>
                        <td>0.299544</td>
                        <td>1.198178</td>
                    </tr>
                    <tr>
                        <td>MPI</td>
                        <td>8</td>
                        <td>7.960175</td>
                        <td>4.430923</td>
                        <td>3.529251</td>
                        <td>0.143796</td>
                        <td>1.150372</td>
                    </tr>
                    <tr>
                        <td>MPI</td>
                        <td>16</td>
                        <td>10.352483</td>
                        <td>5.47187</td>
                        <td>4.880611</td>
                        <td>0.055284</td>
                        <td>0.884538</td>
                    </tr>
                </tbody>
            </table>
            <img src="chart_5.png" />
            <h2 id="discussion">Discussion</h2>            
            <p>In implementing the above algorithms in serial, MPI, and MapReduce, we found some compelling results. Primarily, the algorithms that required crawling the entire graph (all pairs shortest paths, for example), enjoyed a large speedup from MPI. On the other hand, some of the quicker algorihms did not experience this same speedup, as shown in the charts above. In addition, the overhead of MapReduce greatly inhibited those algorithms, causing them to run slower then their MPI counterparts. </p>
            <p>In our MPI code in particular, we found many opportunities for optimization over our initial implementation. For example, in our graph generation program, we initially distributed the different Discuss threads to the MPI processes, computed partial subgraphs, then sent all the subgraphs back to the root process to be compiled into a final graph. Iterating over the subgraphs took nearly as much time as generating the intial graphs in serial, so we decided to update our communication strategy. We came up with the tree based strategy described above, whereby subgraphs can be reduced into larger subgraphs in parallel, limiting the need for more additions. We found that this communication strategy performed faster, being effectively "more parallel".</p>
            <p>Processing the social network graph in parallel led to some insightful results. Surprisingly, the shortest path between two users was very often less than 3 nodes, and never larger than 5. Having processed the social network graph quickly, we were inspired to tackle larger sets of data, namely the action data. Processing the action data (using our modified EdgeRank algorithm) allowed us to generate both the friendship affinity scores and Newsfeed applications. </p>
            <p>In working with this project, we thoroughly enjoyed the relevancy of the project domain to our daily lives. We have spent the past three months answering questions on the CS50 discussion board, and being able to gain insights into the web beneath it was certainly exciting. In addition, we enjoyed programming our algorithms in both serial and parallel, and acheiving the speedups we did. Especially seeing an algorithm like "all-pairs shortest paths" become much faster was quite exciting. Finally, designing our own EdgeRank algorithms for the friendship scores and newsfeed was intellectually stimulating. We read publications from Google and Facebook, and got to experiment with different parameters and methods. We also designed the algorithm with parallelization in mind, which was a first, as most algorithms are written serially, and then converted to parallel versions.</p>
            <p>It was challenging to parallelize the graph generation code. At first, we believed that it simply could not be done in parallel, as each graph needed to know about the other subgraphs. When we realized, however, that each Discuss thread was effectively its own subgraph, we realized that we could in fact parallelize the graph generation. Even then, however, we had difficulty effectively dividing the work across MPI processes, and quickly combining the results. The fruit of our efforts with this problem is the tree-like reduction pattern depicted and described above. On a practical side, we also had difficulty finding a front-end graphics library that could display the number of nodes/edges we were dealing with. After experimenting with d3, we ultimately found sigma.js, which served our purposes nicely. Finally, after writing our programs to output JSON descriptions of the graphs, we found that communicating this JSON to the web browser was taking too long. To circumvent this issue, we instead write our graph into a sqlite database, so that we can issue AJAX requests asking for the weights we need. This method became especially important with the shortest paths, where each pair of nodes needs to record the length of the path, and the path itself. </p>
            <h2 id="future">Future Work</h2>
            <p>Our current parallelization methods take advantage of MapReduce and MPI. In the future, we would also like to compare CUDA-based implementations of some of the programs we've written. Problems such as the parallelized graph construction may be well suited for a CUDA kernel, particularly if the resulting graph has a large number of nodes. Similarly, the extreme parallelism of CUDA could also us to concurrently compute a much higher number of shortest paths, which may improve performance on that subproblem.</p>
            <p>We would also like to implement additional functionality into our visualizations. For example, given the size of the social network graph, we would like to make it easier for users to find trends in discussions. For example, it is likely that the board contains clusters of discussions, in which subsets of users commonly interact with each other. Our current visualization makes it difficult for users to identify these clusters, as this would require hovering over nodes individually. Instead, we would like to determine the location of these clusters programmatically.</p>
            <p>In addition, our sentence predictor currently uses pairs of adjacent words as a data set. We would also like to explore the effects of considering triplets of words. While many CS50 topics, like "linked lists" and "pointer arithmetic," are two words, taking trigrams into account may help the sentence predictor identify other common phrases like "structured query language."</p>
            <h2 id="screenshots">Screenshots</h2>
            <h3>Social Network Graph</h3>
            <img src="social_graph_screenshot.png"/><br/>
            <h3>Sentence Prediction</h3>
            <img src="sentence_predictor_screenshot.png"/>
        </div>
    </body>
</html>
