$(function() {
    // get data
    $.getJSON('../output/suggested_friends.json', function(scores) {
        $.getJSON('../output/users.json', function(users) {
            // build select box
            var html = '<option value=""></option>';
            for (var i in scores)
                html += '<option value="' + i + '">' + users.users[i] + '</option>';
            $('#users').html(html);

            $('#users').on('change', function() {
                $.getJSON('newsfeed.php?id=' + $(this).val(), function(feed) {
                    var html = '';
                    for (var i in feed) {
                        var story = feed[i];
                        var verb = '';
                        if (story.action == 'watch')
                            verb = ' watched a post by ';
                        else if (story.action == 'read')
                            verb = ' read a post by ';
                        else if (story.action == 'reply')
                            verb = ' replied to a post by ';
                        else if (story.action == 'vote')
                            verb = ' voted for a reply by ';

                        html += '<li>' + users.users[story.source] + verb + users.users[story.target] + 
                            ' (' + story.timestamp + ')';
                    }

                    $('#feed').html(html);
                });
            });
        });
    });
});
