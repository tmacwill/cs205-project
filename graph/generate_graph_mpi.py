import sqlite3
import json
from collections import defaultdict
import math
import numpy as np
import sys
from mpi4py import MPI
import time

verbose = False

def process_thread(mode, nodes, edges):
    """
    Process a thread for either users or words
    """

    if mode == 'users':
        return process_thread_users(nodes, edges)
    elif mode == 'words':
        return process_thread_words(nodes, edges)


def process_thread_users(nodes, edges):
    """
    Nodes are users and edges are interactions between users
    """

    # iterate over all pairs of users
    for node in thread:
        if node and node != 'null':
            nodes.add(node)
            for n in thread:
                if node != n and n and n != 'null':
                    # make sure elements exist in dictionary
                    if not node in edges:
                        edges[node] = {}
                    if not n in edges[node]:
                        edges[node][n] = 0

                    # increment number of connections
                    edges[node][n] += 1


def process_thread_words(nodes, edges):
    """
    Nodes are words and edges are connections between words
    """

    # get all words in the thread
    words = []
    for node in thread:
        if node:
            words = words + node.split(' ')

    # iterate over all pairs of words
    for w1 in words:
        if w1 and w1 != 'null':
            nodes.add(w1)
            for w2 in words: 
                if w1 != w2 and w2 and w2 != 'null':
                    # make sure elements exist in dictionary
                    if not w1 in edges:
                        edges[w1] = {}
                    if not w2 in edges[w1]:
                        edges[w1][w2] = 0

                    # increment number of connections
                    edges[w1][w2] += 1


def vprint(text, args = None):
    """
    Debug printing
    """
    global verbose

    if not verbose:
        return
    if args != None:
        print text % args
    else:
        print text


# initialize MPI
comm = MPI.COMM_WORLD
rank = comm.Get_rank()
size = comm.Get_size()

# make sure enough arguments are given
if len(sys.argv) < 3:
    if rank == 0:
        print "Usage: mpirun -n [2|4|8] python generate_graph_mpi.py [users|words] db [min_threshold] [max_threshold]"
    sys.exit()

# parse command-line arguments
mode = sys.argv[1]
db_file = sys.argv[2]
min_threshold = 0
max_threshold = sys.maxint
if len(sys.argv) > 3:
    min_threshold = int(sys.argv[3])
if len(sys.argv) > 4:
    max_threshold = int(sys.argv[4])

# start timer
comm.barrier()
parallel_start_time = MPI.Wtime()

# connect to sqlite
con = sqlite3.connect(db_file + ".db")
db = con.cursor()

# get the total number of posts
db.execute("select count(id) from posts")
post_count = db.fetchone()[0]

# each process handles a subset of threads
n = int(np.ceil(float(post_count) / size))
start = n * rank
end = n * rank + n
if rank == size - 1:
    end = post_count + 1

# query for user data for social network
query = ''
if mode == 'users':
    query = "select posts.id, posts.user_id, replies.user_id from posts left outer join replies on posts.id = replies.post_id where posts.id >= %d and posts.id < %d" % (start, end)

# query for post data for word graph
elif mode == 'words':
    query = "select posts.id, posts.title, posts.content, replies.content from posts left outer join replies on posts.id = replies.post_id where posts.id >= %d and posts.id < %d" % (start, end)

# run data-specific query
db.execute(query)
data = db.fetchall()

# process each thread
thread = set()
nodes = set()
edges = {}
previous_id = 0
for i in data:
    # if we have a new thread ID, then we have accumulated all replies, so we can process the thread
    if previous_id != i[0]:
        process_thread(mode, nodes, edges)
        thread = set()

        if mode == 'users':
            thread.add(i[1])
        elif mode == 'words':
            thread.add(i[1])
            thread.add(i[2])

    # accumulate reply data
    if mode == 'users':
        thread.add(i[2])
    elif mode == 'words':
        thread.add(i[3])

    # remember thread id
    previous_id = i[0]

# handle the final thread
process_thread(mode, nodes, edges)

# flatten the 2d edge list into a 1d list
nodes_list = list(nodes)

summed_edges = edges

# using a log(n) reduction, consolidate subgraphs into a single adjacency list
for i in xrange(1, int(math.log(size, 2) + 1)):
    # after sending a subgraph once, process is done
    if rank % 2**(i-1) == 0:
        # send subgraph to adjacent active process
        if rank % 2**i != 0 and rank-2**(i-1) >= 0:
            comm.send(summed_edges, dest=rank-2**(i-1))
        # receive subgraph from adjacent active process
        elif rank+2**(i-1) < size:
            recv_edges = comm.recv(source=rank+2**(i-1))
            # combine subgraphs by summing edges
            for source in recv_edges:
                edges = recv_edges[source]
                for target in edges:
                    if not source in summed_edges:
                        summed_edges[source] = {}
                    if not target in summed_edges[source]:
                        summed_edges[source][target] = 0
                    summed_edges[source][target] += recv_edges[source][target]
    # make sure all processes finish before moving on
    comm.barrier()

# gather all nodes at once
gathered_nodes = comm.gather(nodes_list)
all_nodes = set()

if rank == 0:
    # total parallel time
    parallel_end_time = MPI.Wtime()
    serial_start_time = MPI.Wtime()

    # compute the set of unique nodes
    for i in gathered_nodes:
        all_nodes |= set(i)

    # format nodes for json output
    formatted_nodes = []
    for node in all_nodes:
        formatted_nodes.append({
            'id': node
        })

    # format edges for json output and compute node weights
    formatted_edges = []
    weights = defaultdict(int)
    for node in summed_edges:
        for n in summed_edges[node]:
            if summed_edges[node][n] >= min_threshold and summed_edges[node][n] <= max_threshold:
                formatted_edges.append({
                    'source': str(node),
                    'target': str(n),
                    'weight': summed_edges[node][n]
                })

            weights[str(node)] += summed_edges[node][n]

    # assign weights to nodes
    for node in formatted_nodes:
        if str(node['id']) in weights:
            node['weight'] = weights[str(node['id'])]
        else:
            node['weight'] = 0

    serial_end_time = MPI.Wtime()
    #print "Total parallel time: %f" % (parallel_end_time - parallel_start_time)
    #print "Total serial time: %f" % (serial_end_time - serial_start_time)
    #print "Total time: %f" % (serial_end_time - parallel_start_time)

    print json.dumps({
        'nodes': formatted_nodes,
        'edges': formatted_edges
    })
