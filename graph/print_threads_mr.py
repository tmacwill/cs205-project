import sqlite3
import json
import sys

'''
Reads a db file and writes to a txt file for processing by MapReduce
In users mode, write a list of all the users that participated in a thread to a line
In words mode, write all the text from a thread to a line
'''

def process_thread(mode):
    text = ""
    for node in thread:
        if node != None:
            text = text + " " + str(node)

    # remove extra formatting and special chars
    text = text.strip()
    text = text.replace("\n", " ")
    text = text.replace("\t", " ")
    text = text.replace("(", " ")
    text = text.replace(")", " ")
    text = ' '.join(text.split())

    # Write the text to a file, with each thread on a separate line
    if len(text) > 0:
        try:
            f.write(text + "\n")
        except UnicodeEncodeError:
            print "Couldn't encode a symbol"

# make sure enough arguments are given
if len(sys.argv) < 4:
    print "Usage: python print_threads_mr.py [users|words] db outfile"
    sys.exit()

# parse command-line arguments
mode = sys.argv[1]
db_file = sys.argv[2]
out_file = sys.argv[3]

# connect to sqlite
con = sqlite3.connect(db_file + ".db")
db = con.cursor()

# query for user data for social network
query = ''
if mode == 'users':
    query = "select posts.id, posts.user_id, replies.user_id from posts left outer join replies on posts.id = replies.post_id"

# query for post data for word graph
elif mode == 'words':
    query = "select posts.id, posts.title, posts.content, replies.content from posts left outer join replies on posts.id = replies.post_id"

# run data-specific query
db.execute(query)
data = db.fetchall()

f = open(out_file, 'w')

# process each thread
nodes = set()
edges = {}
previous_id = 0
thread = set()
for i in data:
    # if we have a new thread ID, then we have accumulated all replies, so we can process the thread
    if i[0] != previous_id:
        process_thread(mode)
        thread = set()

        # add post data
        if mode == 'users':
            thread.add(i[1])
        elif mode == 'words':
            thread.add(i[1])
            thread.add(i[2])

    # accumulate reply data
    if mode == 'users':
        thread.add(i[2])
    elif mode == 'words':
        thread.add(i[3])

    # remember thread id
    previous_id = i[0]

# handle the final thread
process_thread(mode)


