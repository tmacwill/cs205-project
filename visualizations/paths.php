<?php

// get source and taret nodes
$source = $_GET['source'];
$target = $_GET['target'];

// query sqlite for path
$db = new PDO('sqlite:../output/paths.db');
$statement = $db->prepare('select path from paths where source = ? and target = ?');
$statement->execute(array($source, $target));
$result = $statement->fetch();

// output path as json
echo json_encode(array(
    'source' => $source,
    'target' => $target,
    'path' => explode(',', $result['path'])
));
