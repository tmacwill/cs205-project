<?php

$type = $_GET['type'];
$db = new PDO('sqlite:../output/sentence_prediction.db');

// get all source words for dropdown
if ($type == 'all') {
    $statement = $db->prepare('select source from sentences');
    $statement->execute();
    $result = $statement->fetchAll();

    // output words as json
    echo json_encode(array(
        'words' => array_map(function($e) { return $e['source']; }, $result)
    ));
}

// query database for next worsd in path
else if ($type == 'word') {
    $source = $_GET['source'];

    // get words following this word
    $statement = $db->prepare('select next_nodes from sentences where source = ?');
    $statement->execute(array($source));
    $result = $statement->fetch();

    // output words as json
    echo json_encode(array(
        'source' => $source,
        'words' => explode(',', $result['next_nodes'])
    ));
}

// query database for next n words
else if ($type == 'lucky') {
    $source = $_GET['source'];
    $n = (int)$_GET['n'];
    $used = array();
    $sentence = array();

    // fetch n words from database
    for ($i = 0; $i < $n; $i++) {
        // get next words
        $statement = $db->prepare('select next_nodes from sentences where source = ?');
        $statement->execute(array($source));
        $result = $statement->fetch();

        // add a random word to the sentence
        $words = explode(',', $result['next_nodes']);
        $word = $words[rand(0, count($words) - 1)];
        $sentence[] = $word;
        $source = $word;
    }

    echo json_encode(array(
        'words' => $sentence
    ));
}
